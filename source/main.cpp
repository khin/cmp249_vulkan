#include <iostream>
#include <stdexcept>

#include "application.hpp"

int main(int argc, char **argv) {
	Application app;

	try {
		app.Run();
	} catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
		return 1;
	}

	return 0;
}
