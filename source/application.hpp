#pragma once

#include <stdexcept>
#include <vector>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vulkan/vulkan.hpp>

#include "geometry.hpp"

const int WIDTH = 1024;
const int HEIGHT = 768;

uint32_t FindMemoryType(uint32_t type_filter, vk::MemoryPropertyFlags properties);

struct VkQueueFamilyIndices {
	int graphics = -1;
	int present = -1;

	VkQueueFamilyIndices();
	VkQueueFamilyIndices(vk::PhysicalDevice, vk::SurfaceKHR);

	inline bool is_complete() {
		return graphics >= 0 && present >= 0;
	}
};

struct SwapchainSupport {
	vk::SurfaceCapabilitiesKHR capabilities;
	std::vector<vk::SurfaceFormatKHR> formats;
	std::vector<vk::PresentModeKHR> present_modes;

	SwapchainSupport();
	SwapchainSupport(vk::PhysicalDevice, vk::SurfaceKHR);
};

class Application {
private:
	GLFWwindow* window;

	const std::vector<const char*> validation_layers = {
		"VK_LAYER_LUNARG_standard_validation"
	};

	const std::vector<const char*> device_extensions = {
		"VK_KHR_swapchain"
	};

	const bool enable_validation_layers =
	#ifdef _DEBUG_ENABLED_
		true;
	#else
		false;
	#endif

	static Application* singleton;

	vk::Instance instance;
	vk::DebugReportCallbackEXT debug_callback;

	vk::PhysicalDevice physical_device;
	vk::Device device;
	vk::Queue queue_graphics;
	vk::Queue queue_present;

	vk::SurfaceKHR surface;

	vk::SwapchainKHR swapchain;
	vk::Format swapchain_format;
	vk::Extent2D swapchain_extent;
	vk::Viewport viewport;
	vk::Rect2D scissor;

	std::vector<vk::Image> swapchain_images;
	std::vector<vk::ImageView> swapchain_imageviews;
	std::vector<vk::Framebuffer> framebuffers;

	vk::Image depth_image;
	vk::ImageView depth_imageview;
	vk::DeviceMemory depth_image_memory;

	vk::RenderPass renderpass;
	vk::CommandPool command_pool;
	std::vector<vk::CommandBuffer> command_buffers;

	vk::Semaphore sem_image_available;
	vk::Semaphore sem_render_finished;

public:
	static vk::PhysicalDevice& get_physical_device();
	static vk::Device& get_device();
	Application();

	void Run();

protected:
	Geometry geometry;

	void InitWindow();
	void InitVulkan();
	void MainLoop();
	void Cleanup();

	/* Vulkan objects */
	bool CheckValidationLayersSupport();
	bool CheckDeviceExtensionSupport(vk::PhysicalDevice);
	bool isDeviceSuitable(vk::PhysicalDevice);
	std::vector<const char*> GetRequiredExtensions();

	void CreateInstance();
	bool SetupDebugCallback();
	bool DestroyDebugCallback();
	void PickPhysicalDevice();
	void CreateLogicalDevice();

	void CreateSurface();
	void CreateSwapchain();
	void CreateSwapchainImageViews();

	void CreateRenderPass();
	void CreateFramebuffers();
	void CreateCommandPool();
	void CreateDepthResources();
	void AllocateCommandBuffers();

	vk::CommandBuffer BeginOneshotCommand();
	void EndOneshotCommand(vk::CommandBuffer);
	void TransitionImageLayout(
		vk::Image,
		vk::Format,
		vk::ImageLayout old_layout,
		vk::ImageLayout new_layout,
		vk::ImageAspectFlags aspect_mask = vk::ImageAspectFlagBits::eColor
	);

	void CreateSemaphores();
	void UpdateUniformBuffer();
	void DrawFrame();
};
