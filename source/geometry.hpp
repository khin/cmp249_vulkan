#pragma once

#include <vector>
#include <array>

#include <vulkan/vulkan.hpp>
#include <glm/glm.hpp>

#include "material.hpp"

struct Vertex {
	glm::vec3 position;
	glm::vec3 color;

	static vk::VertexInputBindingDescription GetBindingDescription();
	static std::array<vk::VertexInputAttributeDescription, 2> GetAttributeDescription();
};

class Geometry {
protected:
	const std::vector<Vertex> vertices = {
		{ { -0.5f, -0.5f, 0.0f }, { 1.0f, 0.0f, 0.0f } },
		{ { 0.5f, -0.5f, 0.0f },  { 0.0f, 1.0f, 0.0f } },
		{ { 0.5f, 0.5f, 0.0f },   { 0.0f, 0.0f, 1.0f } },
		{ { -0.5f, 0.5f, 0.0f },  { 1.0f, 1.0f, 1.0f } },

		{ { -0.5f, -0.5f, -0.5f }, { 1.0f, 0.0f, 0.0f } },
		{ { 0.5f, -0.5f, -0.5f },  { 0.0f, 1.0f, 0.0f } },
		{ { 0.5f, 0.5f, -0.5f },   { 0.0f, 0.0f, 1.0f } },
		{ { -0.5f, 0.5f, -0.5f },  { 1.0f, 1.0f, 1.0f } }
	};

	const std::vector<uint16_t> indices = {
		0, 1, 2, 2, 3, 0,
		4, 5, 6, 6, 7, 4
	};

	vk::Buffer vertex_buffer;
	vk::Buffer index_buffer;

	vk::DeviceMemory vertex_buffer_memory;
	vk::DeviceMemory index_buffer_memory;

public:
	Material material;

	void CreateVertexBuffer();
	void CreateIndexBuffer();
	void RecordDrawingCommands(vk::CommandBuffer&);

	void Cleanup();
};
