#include "shader.hpp"

#include <stdexcept>
#include <fstream>
#include "application.hpp"

#include <iostream>
using namespace std;

using std::vector;
using std::string;

static vector<char> read_file(const string& filename) {
	std::ifstream file(filename, std::ios::ate | std::ios::binary);

	if (not file.is_open()) {
		throw std::runtime_error("Failed to open file: " + filename);
	}

	size_t size = (size_t)file.tellg();
	vector<char> buffer(size);

	file.seekg(0);
	file.read(buffer.data(), size);
	file.close();

	return buffer;
}

Shader::Shader() {}

Shader::Shader(
	const string& filename_vertex_shader,
	const string& filename_fragment_shader
) {
	LoadVertexShader(filename_vertex_shader);
	LoadFragmentShader(filename_fragment_shader);
}

void Shader::LoadVertexShader(const string& filename) {
	code_vertex = read_file(filename);
	module_vertex = CreateModule(code_vertex);
}

void Shader::LoadFragmentShader(const string& filename) {
	code_fragment = read_file(filename);
	module_fragment = CreateModule(code_fragment);
}

vk::ShaderModule Shader::CreateModule(const vector<char> &code) {
	vk::Device device = Application::get_device();

	vk::ShaderModuleCreateInfo module_info;
	module_info.codeSize = code.size();
	module_info.pCode = reinterpret_cast<const uint32_t*>(code.data());

	vk::ShaderModule module;
	module = device.createShaderModule(module_info);

	if (!module) {
		throw std::runtime_error("Failed to create shader module");
	}

	return module;
}

void Shader::DestroyModules() {
	vk::Device device = Application::get_device();

	device.destroyShaderModule(module_vertex);
	device.destroyShaderModule(module_fragment);
}
