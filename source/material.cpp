#include "material.hpp"
#include "application.hpp"
#include "geometry.hpp"

#include <array>
#include <iostream>

using std::string;

void Material::SetShader(const Shader &shader) {
	this->shader = shader;
}

Shader& Material::GetShader() {
	return shader;
}

void Material::SetVertexShaderEntryPoint(const string &entry_point) {
	entrypoint_vertex = entry_point;
}

void Material::SetFragmentShaderEntryPoint(const string &entry_point) {
	entrypoint_fragment = entry_point;
}

void Material::CreateDescriptorSetLayout() {
	vk::DescriptorSetLayoutBinding ubo_binding;
	ubo_binding.binding = 0;
	ubo_binding.descriptorType = vk::DescriptorType::eUniformBuffer;
	ubo_binding.descriptorCount = 1;
	ubo_binding.stageFlags = vk::ShaderStageFlagBits::eVertex;

	vk::DescriptorSetLayoutCreateInfo layout_info;
	layout_info.bindingCount = 1;
	layout_info.pBindings = &ubo_binding;

	auto device = Application::get_device();
	descriptor_set_layout = device.createDescriptorSetLayout(layout_info);
}

void Material::CreateUniformBuffer() {
	vk::BufferCreateInfo buffer_info;
	buffer_info.size = sizeof(UniformBufferObject);
	buffer_info.usage = vk::BufferUsageFlagBits::eUniformBuffer;
	buffer_info.sharingMode = vk::SharingMode::eExclusive;

	auto device = Application::get_device();
	uniform_buffer = device.createBuffer(buffer_info);

	vk::MemoryRequirements mem_requirements;
	mem_requirements = device.getBufferMemoryRequirements(uniform_buffer);

	vk::MemoryAllocateInfo alloc_info;
	alloc_info.allocationSize = mem_requirements.size;
	alloc_info.memoryTypeIndex = FindMemoryType(
		mem_requirements.memoryTypeBits,
		vk::MemoryPropertyFlagBits::eHostVisible |
		vk::MemoryPropertyFlagBits::eHostCoherent
	);

	uniform_buffer_memory = device.allocateMemory(alloc_info);
	device.bindBufferMemory(uniform_buffer, uniform_buffer_memory, 0);
}

void Material::CreateDescriptorPool() {
	vk::DescriptorPoolSize pool_size;
	pool_size.type = vk::DescriptorType::eUniformBuffer;
	pool_size.descriptorCount = 1;

	vk::DescriptorPoolCreateInfo pool_info;
	pool_info.poolSizeCount = 1;
	pool_info.pPoolSizes = &pool_size;
	pool_info.maxSets = 1;

	auto device = Application::get_device();
	descriptor_pool = device.createDescriptorPool(pool_info);
}

void Material::CreateDescriptorSet() {
	vk::DescriptorSetLayout layouts[] = { descriptor_set_layout };
	vk::DescriptorSetAllocateInfo alloc_info;
	alloc_info.descriptorPool = descriptor_pool;
	alloc_info.descriptorSetCount = 1;
	alloc_info.pSetLayouts = layouts;

	auto device = Application::get_device();
	descriptor_set = device.allocateDescriptorSets(alloc_info)[0];

	vk::DescriptorBufferInfo buffer_info;
	buffer_info.buffer = uniform_buffer;
	buffer_info.offset = 0;
	buffer_info.range = sizeof(UniformBufferObject);

	vk::WriteDescriptorSet descriptor_write;
	descriptor_write.dstSet = descriptor_set;
	descriptor_write.dstBinding = 0;
	descriptor_write.dstArrayElement = 0;
	descriptor_write.descriptorType = vk::DescriptorType::eUniformBuffer;
	descriptor_write.descriptorCount = 1;
	descriptor_write.pBufferInfo = &buffer_info;

	device.updateDescriptorSets({ descriptor_write }, {});
}

void Material::CreateGraphicsPipeline(
	const std::vector<vk::Viewport> &viewports,
	const std::vector<vk::Rect2D>   &scissors,
	vk::RenderPass renderpass,
	bool enable_multisampling,
	vk::SampleCountFlagBits sample_count

) {
	vk::Device device = Application::get_device();

	vk::PipelineShaderStageCreateInfo vert_info;
	vert_info.stage = vk::ShaderStageFlagBits::eVertex;
	vert_info.module = shader.module_vertex;
	vert_info.pName = entrypoint_vertex.c_str();

	vk::PipelineShaderStageCreateInfo frag_info;
	frag_info.stage = vk::ShaderStageFlagBits::eFragment;
	frag_info.module = shader.module_fragment;
	frag_info.pName = entrypoint_fragment.c_str();

	vk::PipelineShaderStageCreateInfo shader_stages[] = {
		vert_info, frag_info
	};

	auto binding_description = Vertex::GetBindingDescription();
	auto attribute_description = Vertex::GetAttributeDescription();

	vk::PipelineVertexInputStateCreateInfo input_info;
	input_info.vertexBindingDescriptionCount = 1;
	input_info.pVertexBindingDescriptions = &binding_description;
	input_info.vertexAttributeDescriptionCount = static_cast<uint32_t>(attribute_description.size());
	input_info.pVertexAttributeDescriptions = attribute_description.data();

	vk::PipelineInputAssemblyStateCreateInfo assembly_info;
	assembly_info.topology = this->topology;
	assembly_info.primitiveRestartEnable = this->primitive_restart_enable;

	vk::PipelineViewportStateCreateInfo viewport_info;
	viewport_info.viewportCount = static_cast<uint32_t>(viewports.size());
	viewport_info.pViewports = viewports.data();
	viewport_info.scissorCount = static_cast<uint32_t>(scissors.size());
	viewport_info.pScissors = scissors.data();

	vk::PipelineRasterizationStateCreateInfo raster_info;
	raster_info.depthClampEnable = false;
	raster_info.rasterizerDiscardEnable = false;
	raster_info.polygonMode = this->polygon_mode;
	raster_info.lineWidth = line_width;
	raster_info.cullMode = this->culling;
	raster_info.frontFace = this->front_face;
	raster_info.depthBiasEnable = false;		// shadow maps

	vk::PipelineMultisampleStateCreateInfo multisampling;
	multisampling.sampleShadingEnable = enable_multisampling,
	multisampling.rasterizationSamples = sample_count;

	vk::PipelineDepthStencilStateCreateInfo depth_info;
	depth_info.depthTestEnable = true;
	depth_info.depthWriteEnable = true;
	depth_info.depthCompareOp = vk::CompareOp::eLess;
	depth_info.depthBoundsTestEnable = false;
	depth_info.stencilTestEnable = false;

	vk::PipelineColorBlendAttachmentState colorblend_attachment;
	colorblend_attachment.colorWriteMask =	vk::ColorComponentFlagBits::eR |
						vk::ColorComponentFlagBits::eG |
						vk::ColorComponentFlagBits::eB |
						vk::ColorComponentFlagBits::eA;
	colorblend_attachment.blendEnable = true;	// alpha blending
	colorblend_attachment.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
	colorblend_attachment.dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
	colorblend_attachment.colorBlendOp = vk::BlendOp::eAdd;
	colorblend_attachment.srcAlphaBlendFactor = vk::BlendFactor::eOne;
	colorblend_attachment.dstAlphaBlendFactor = vk::BlendFactor::eZero;
	colorblend_attachment.alphaBlendOp = vk::BlendOp::eAdd;

	vk::PipelineColorBlendStateCreateInfo colorblending;
	colorblending.logicOpEnable = false;
	colorblending.logicOp = vk::LogicOp::eCopy;
	colorblending.attachmentCount = 1;
	colorblending.pAttachments = &colorblend_attachment;
	colorblending.blendConstants[0] = 0.0f;
	colorblending.blendConstants[1] = 0.0f;
	colorblending.blendConstants[2] = 0.0f;
	colorblending.blendConstants[3] = 0.0f;

	vk::PipelineLayoutCreateInfo layout_info;
	layout_info.setLayoutCount = 1;
	layout_info.pSetLayouts = &descriptor_set_layout;
	layout_info.pushConstantRangeCount = 0;
	layout_info.pPushConstantRanges = nullptr;

	pipeline_layout = device.createPipelineLayout(layout_info);
	if (!pipeline_layout) {
		throw std::runtime_error("Failed to create pipeline layout");
	}

	vk::GraphicsPipelineCreateInfo pipeline_info;
	pipeline_info.stageCount = 2;
	pipeline_info.pStages    = shader_stages;
	pipeline_info.pVertexInputState   = &input_info;
	pipeline_info.pInputAssemblyState = &assembly_info;
	pipeline_info.pViewportState      = &viewport_info;
	pipeline_info.pRasterizationState = &raster_info;
	pipeline_info.pMultisampleState   = &multisampling;
	pipeline_info.pDepthStencilState  = &depth_info;
	pipeline_info.pColorBlendState    = &colorblending;
	pipeline_info.pDynamicState       = nullptr; // not used for now
	pipeline_info.layout     = pipeline_layout;
	pipeline_info.renderPass = renderpass;
	pipeline_info.subpass    = 0;	// index of subpass inside renderpass
	pipeline_info.basePipelineHandle = nullptr;
	pipeline_info.basePipelineIndex  = -1;

	pipeline = device.createGraphicsPipeline(nullptr, pipeline_info);
}

vk::Pipeline& Material::GetPipeline() {
	return pipeline;
}

void Material::Cleanup() {
	vk::Device device = Application::get_device();

	device.destroyDescriptorPool(descriptor_pool);

	device.destroyDescriptorSetLayout(descriptor_set_layout);
	device.destroyBuffer(uniform_buffer);
	device.freeMemory(uniform_buffer_memory);

	device.destroyPipeline(pipeline);
	device.destroyPipelineLayout(pipeline_layout);
}
