#include "application.hpp"

#include <iostream>
#include <string>
#include <set>
#include <algorithm>
#include <array>
#include <chrono>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "shader.hpp"

using std::vector;
using std::array;
using std::string;

// ##################################################

uint32_t FindMemoryType(uint32_t type_filter, vk::MemoryPropertyFlags properties) {
	vk::PhysicalDeviceMemoryProperties mem_properties;
	mem_properties = Application::get_physical_device().getMemoryProperties();

	for (uint32_t i = 0; i < mem_properties.memoryTypeCount; i++) {
		if (
			type_filter & (1 << i) and
			(mem_properties.memoryTypes[i].propertyFlags & properties) == properties
		) {
			return i;
		}
	}

	throw std::runtime_error("Failed to find suitable memory type");
}

// ##################################################

VkQueueFamilyIndices::VkQueueFamilyIndices() {}

VkQueueFamilyIndices::VkQueueFamilyIndices(vk::PhysicalDevice device, vk::SurfaceKHR surface) {
	vector<vk::QueueFamilyProperties> families = device.getQueueFamilyProperties();

	int i = 0;
	for (const auto &family : families) {
		if (family.queueCount > 0 && family.queueFlags & vk::QueueFlagBits::eGraphics) {
			graphics = i;
		}

		bool present_support = device.getSurfaceSupportKHR(i, surface);
		if (family.queueCount > 0 && present_support) {
			present = i;
		}

		if (is_complete()) break;

		i++;
	}
}

// ##################################################

SwapchainSupport::SwapchainSupport(){};

SwapchainSupport::SwapchainSupport(vk::PhysicalDevice device, vk::SurfaceKHR surface) {
	formats       = device.getSurfaceFormatsKHR(surface);
	capabilities  = device.getSurfaceCapabilitiesKHR(surface);
	present_modes = device.getSurfacePresentModesKHR(surface);
}

// ##################################################

Application* Application::singleton = nullptr;

Application::Application() {
	singleton = this;
}

void Application::Run() {
	InitWindow();
	InitVulkan();

	MainLoop();

	Cleanup();
}

void Application::InitWindow() {
	glfwInit();

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	window = glfwCreateWindow(WIDTH, HEIGHT, "CMP249 - Vulkan Demo", nullptr, nullptr);
}

void Application::InitVulkan() {
	CreateInstance();
	SetupDebugCallback();
	CreateSurface();

	PickPhysicalDevice();
	CreateLogicalDevice();
	CreateSwapchain();
	CreateSwapchainImageViews();

	CreateRenderPass();

	Shader shader;
	shader.LoadVertexShader("shaders/shader-vert.spv");
	shader.LoadFragmentShader("shaders/shader-frag.spv");

	geometry.material.SetShader(shader);
	geometry.material.CreateDescriptorSetLayout();

	geometry.material.front_face = vk::FrontFace::eCounterClockwise;
	geometry.material.CreateGraphicsPipeline({ viewport }, { scissor }, renderpass);
	shader.DestroyModules();

	CreateCommandPool();
	CreateDepthResources();
	CreateFramebuffers();

	geometry.CreateVertexBuffer();
	geometry.CreateIndexBuffer();
	geometry.material.CreateUniformBuffer();
	geometry.material.CreateDescriptorPool();
	geometry.material.CreateDescriptorSet();

	AllocateCommandBuffers();

	CreateSemaphores();
}

void Application::MainLoop() {
	while (!glfwWindowShouldClose(window)) {
		glfwPollEvents();

		UpdateUniformBuffer();
		DrawFrame();
	}
}

void Application::Cleanup() {
	device.waitIdle();

	device.destroyImageView(depth_imageview);
	device.destroyImage(depth_image);
	device.freeMemory(depth_image_memory);

	device.destroySemaphore(sem_image_available);
	device.destroySemaphore(sem_render_finished);

	device.destroyCommandPool(command_pool);

	for (auto framebuffer : framebuffers) { device.destroyFramebuffer(framebuffer); }
	framebuffers.clear();

	geometry.Cleanup();

	device.destroyRenderPass(renderpass);

	for (auto imageview : swapchain_imageviews) { device.destroyImageView(imageview); }
	swapchain_imageviews.clear();

	device.destroySwapchainKHR(swapchain);
	device.destroy();

	DestroyDebugCallback();
	instance.destroySurfaceKHR(surface);
	instance.destroy();

	glfwDestroyWindow(window);
	glfwTerminate();
}

// ##################################################

vector<const char*> Application::GetRequiredExtensions() {
	vector<const char*> extensions;

	uint32_t glfw_extension_count = 0;
	const char** glfw_extensions;
	glfw_extensions = glfwGetRequiredInstanceExtensions(&glfw_extension_count);

	for (uint32_t i = 0; i < glfw_extension_count; i++) {
		extensions.push_back(glfw_extensions[i]);
	}

	if (enable_validation_layers) {
		extensions.push_back("VK_EXT_debug_report");
	}

	return extensions;
}

void Application::CreateInstance() {
	if (enable_validation_layers and not CheckValidationLayersSupport()) {
		throw std::runtime_error("Validation layers requested are not available");
	}

	vk::ApplicationInfo app_info;
	app_info.pApplicationName = "CMP249 Vulkan Demo";
	app_info.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
	app_info.pEngineName = "None";
	app_info.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	app_info.apiVersion = VK_API_VERSION_1_0;

	vector<const char*> extensions = GetRequiredExtensions();
	vk::InstanceCreateInfo instance_info;
	instance_info.pApplicationInfo = &app_info;
	instance_info.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
	instance_info.ppEnabledExtensionNames = extensions.data();
	instance_info.enabledLayerCount = 0;

	if (enable_validation_layers){
		instance_info.enabledLayerCount = static_cast<uint32_t>(validation_layers.size());
		instance_info.ppEnabledLayerNames = validation_layers.data();
	}

	instance = vk::createInstance(instance_info);
}

bool Application::CheckValidationLayersSupport() {
	vector<vk::LayerProperties> available_layers;
	available_layers = vk::enumerateInstanceLayerProperties();

	for (const char *layer_name : validation_layers) {
		bool layer_found = false;

		for (const auto &layer_properties : available_layers) {
			if (strcmp(layer_name, layer_properties.layerName) == 0) {
				layer_found = true;
				break;
			}
		}

		if (!layer_found) {
			return false;
		}
	}

	return true;
}

static VKAPI_ATTR VkBool32 VKAPI_CALL _vk_debug_print(
		VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType,
		uint64_t obj, size_t location, int32_t code,
		const char *layerPrefix, const char *msg, void *userData) {

	std::cerr <<	"VULKAN ERROR: Flags: " << vk::to_string(vk::DebugReportFlagBitsEXT(flags)) <<
			"\tObject type: " << vk::to_string(vk::DebugReportObjectTypeEXT(objType)) <<
			"\tCode: " << code <<
			"\tMessage: \"" << msg << "\"" <<
			std::endl;
}

bool Application::SetupDebugCallback() {
	if (!enable_validation_layers) return true;

	vk::DebugReportCallbackCreateInfoEXT debug_info = {};
	debug_info.flags =	vk::DebugReportFlagBitsEXT::eError |
				vk::DebugReportFlagBitsEXT::eWarning |
				vk::DebugReportFlagBitsEXT::ePerformanceWarning;
	debug_info.pfnCallback = _vk_debug_print;

	auto vkCreateDebugReportCallbackEXT = (PFN_vkCreateDebugReportCallbackEXT)
			vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");

	if (vkCreateDebugReportCallbackEXT == nullptr) {
		std::cerr << "Failed to find procedure \"vkCreateDebugReportCallbackEXT\"" << std::endl;
		return false;
	}

	vkCreateDebugReportCallbackEXT(
			instance, (VkDebugReportCallbackCreateInfoEXT *)&debug_info,
			nullptr, (VkDebugReportCallbackEXT *)&debug_callback);

	return true;
}

bool Application::DestroyDebugCallback() {
	auto vkDestroyDebugReportCallbackEXT = (PFN_vkDestroyDebugReportCallbackEXT)
			vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");

	if (vkDestroyDebugReportCallbackEXT == nullptr) {
		std::cerr << "Failed to find procedure \"vkDestroyDebugReportCallbackEXT\"" << std::endl;
		return false;
	}

	vkDestroyDebugReportCallbackEXT(instance, (VkDebugReportCallbackEXT)debug_callback, nullptr);
	return true;
}

// ##################################################

bool Application::isDeviceSuitable(vk::PhysicalDevice device) {
	bool extensions_supported = CheckDeviceExtensionSupport(device);
	VkQueueFamilyIndices indices(device, surface);

	bool swapchain_adequate = false;
	if (extensions_supported) {
		SwapchainSupport swapchain_support(device, surface);
		swapchain_adequate =
			not swapchain_support.formats.empty() and
			not swapchain_support.present_modes.empty();
	}

	return indices.is_complete() and extensions_supported and swapchain_adequate;
}

bool Application::CheckDeviceExtensionSupport(vk::PhysicalDevice device) {
	vector<vk::ExtensionProperties> available_extensions;
	available_extensions = device.enumerateDeviceExtensionProperties();

	std::set<string> required_extensions(device_extensions.begin(), device_extensions.end());

	for (const auto &extension : available_extensions) {
		required_extensions.erase(extension.extensionName);
	}

	return required_extensions.empty();
}

void Application::PickPhysicalDevice() {
	vector<vk::PhysicalDevice> devices;
	devices = instance.enumeratePhysicalDevices();

	if (devices.empty()) {
		throw std::runtime_error("Failed to find GPU with Vulkan support!");
	}

	for (const auto& device : devices) {
		if (isDeviceSuitable(device)) {
			physical_device = device;
			break;
		}
	}

	if (!physical_device) {
		throw std::runtime_error("Failed to find a suitable GPU");
	}
}

vk::PhysicalDevice& Application::get_physical_device() {
	if (singleton == nullptr) {
		throw std::runtime_error("Attempted to get device before app creation");
	}

	return singleton->physical_device;
}

void Application::CreateLogicalDevice() {
	VkQueueFamilyIndices indices(physical_device, surface);

	vector<vk::DeviceQueueCreateInfo> queue_infos;
	std::set<int> unique_queue_families = { indices.graphics, indices.present };

	float queue_priority = 1.0;
	for (int queue_family : unique_queue_families) {
		vk::DeviceQueueCreateInfo queue_info;
		queue_info.queueFamilyIndex = queue_family;
		queue_info.queueCount = 1;
		queue_info.pQueuePriorities = &queue_priority;

		queue_infos.push_back(queue_info);
	}

	vk::PhysicalDeviceFeatures device_features = physical_device.getFeatures();
	vk::PhysicalDeviceFeatures desired_features;
	//desired_features.sampleRateShading = device_features.sampleRateShading; // enables multisampling if present

	vk::DeviceCreateInfo device_info;
	device_info.queueCreateInfoCount = static_cast<uint32_t>(queue_infos.size());
	device_info.pQueueCreateInfos = queue_infos.data();
	device_info.pEnabledFeatures = &desired_features;
	device_info.enabledExtensionCount = static_cast<uint32_t>(device_extensions.size());
	device_info.ppEnabledExtensionNames = device_extensions.data();
	device_info.enabledLayerCount = 0;

	if (enable_validation_layers) {
		device_info.enabledLayerCount = static_cast<uint32_t>(validation_layers.size());
		device_info.ppEnabledLayerNames = validation_layers.data();
	}

	device = physical_device.createDevice(device_info);
	queue_graphics = device.getQueue(indices.graphics, 0);
	queue_present  = device.getQueue(indices.present, 0);
}

vk::Device& Application::get_device() {
	if (singleton == nullptr) {
		throw std::runtime_error("Attempted to get device before app creation");
	}

	return singleton->device;
}

void Application::CreateSurface() {
	auto result = glfwCreateWindowSurface(instance, window, nullptr, (VkSurfaceKHR*)&surface);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Failed to create window surface");
	}
}

// ##################################################

static vk::SurfaceFormatKHR ChooseSwapchainSurfaceFormat(const vector<vk::SurfaceFormatKHR>& formats) {
	if (formats.empty()) return {};

	if (formats.size() == 1 and formats[0].format == vk::Format::eUndefined) {
		return { vk::Format::eB8G8R8A8Unorm, vk::ColorSpaceKHR::eSrgbNonlinear };
	}

	for (const auto& format : formats) {
		if (
			format.format == vk::Format::eB8G8R8A8Unorm and
			format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear
		) {
			return format;
		}
	}

	return formats[0];
}

static vk::PresentModeKHR ChooseSwapchainPresentMode(const vector<vk::PresentModeKHR>& modes) {
	for (const auto& mode : modes) {
		if (mode == vk::PresentModeKHR::eMailbox) {
			return mode;
		}
	}

	return vk::PresentModeKHR::eFifo;
}

static vk::Extent2D ChooseSwapchainExtent(const vk::SurfaceCapabilitiesKHR& capabilities) {
	if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
		return capabilities.currentExtent;
	}

	vk::Extent2D extent = { WIDTH, HEIGHT };
	extent.width = std::max(
		capabilities.minImageExtent.width,
		std::min(
			capabilities.maxImageExtent.width,
			extent.width
		)
	);

	extent.height = std::max(
		capabilities.minImageExtent.height,
		std::min(
			capabilities.maxImageExtent.height,
			extent.height
		)
	);

	return extent;
}

void Application::CreateSwapchain() {
	SwapchainSupport support(physical_device, surface);

	vk::SurfaceFormatKHR surface_format = ChooseSwapchainSurfaceFormat(support.formats);
	vk::PresentModeKHR present_mode = ChooseSwapchainPresentMode(support.present_modes);
	swapchain_extent = ChooseSwapchainExtent(support.capabilities);
	swapchain_format = surface_format.format;

	uint32_t image_count = support.capabilities.minImageCount + 1; // triple buffering if available
	if (support.capabilities.maxImageCount > 0 and support.capabilities.maxImageCount < image_count) {
		// if max image count is 0, there is no limit (besides memory)
		image_count = support.capabilities.maxImageCount;
	}

	VkQueueFamilyIndices indices(physical_device, surface);
	uint32_t queue_families[] = { (uint32_t)indices.graphics, (uint32_t)indices.present };

	vk::SwapchainCreateInfoKHR swapchain_info;
	swapchain_info.surface = surface;
	swapchain_info.minImageCount = image_count;
	swapchain_info.imageFormat = surface_format.format;
	swapchain_info.imageColorSpace = surface_format.colorSpace;
	swapchain_info.imageExtent = swapchain_extent;
	swapchain_info.imageArrayLayers = 1; // >1 for stereoscopic 3D (VR)
	swapchain_info.imageUsage = vk::ImageUsageFlagBits::eColorAttachment;
	swapchain_info.imageSharingMode = vk::SharingMode::eExclusive;

	if (indices.graphics != indices.present) {
		swapchain_info.imageSharingMode = vk::SharingMode::eConcurrent;
		swapchain_info.queueFamilyIndexCount = 2;
		swapchain_info.pQueueFamilyIndices = queue_families;
	}

	swapchain_info.preTransform = support.capabilities.currentTransform;
	swapchain_info.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
	swapchain_info.clipped = true;
	swapchain_info.presentMode = present_mode;
	swapchain_info.oldSwapchain = nullptr;

	swapchain = device.createSwapchainKHR(swapchain_info);
	if (!swapchain) {
		throw std::runtime_error("Failed to create swapchain");
	}

	swapchain_images = device.getSwapchainImagesKHR(swapchain);

	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width  = (float) swapchain_extent.width;
	viewport.height = (float) swapchain_extent.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	scissor.offset = vk::Offset2D(0, 0);
	scissor.extent = swapchain_extent;
}

void Application::CreateSwapchainImageViews() {
	swapchain_imageviews.resize(swapchain_images.size());
	for (size_t i = 0; i < swapchain_images.size(); i++) {
		vk::ImageViewCreateInfo imageview_info;
		imageview_info.image    = swapchain_images[i];
		imageview_info.format   = swapchain_format;
		imageview_info.viewType = vk::ImageViewType::e2D;

		imageview_info.components.r = vk::ComponentSwizzle::eIdentity;
		imageview_info.components.g = vk::ComponentSwizzle::eIdentity;
		imageview_info.components.b = vk::ComponentSwizzle::eIdentity;
		imageview_info.components.a = vk::ComponentSwizzle::eIdentity;

		imageview_info.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
		imageview_info.subresourceRange.baseMipLevel = 0;
		imageview_info.subresourceRange.levelCount = 1;
		imageview_info.subresourceRange.baseArrayLayer = 0;
		imageview_info.subresourceRange.layerCount = 1;

		swapchain_imageviews[i] = device.createImageView(imageview_info);

		if (!swapchain_imageviews[i]) {
			throw std::runtime_error("Failed to create swapchain imageview " + std::to_string(i));
		}
	}
}

void Application::CreateRenderPass() {
	vk::AttachmentDescription color_attachment;
	color_attachment.format  = swapchain_format;
	color_attachment.samples = vk::SampleCountFlagBits::e1;
	color_attachment.loadOp  = vk::AttachmentLoadOp::eClear;
	color_attachment.storeOp = vk::AttachmentStoreOp::eStore;
	color_attachment.stencilLoadOp  = vk::AttachmentLoadOp::eDontCare;
	color_attachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
	color_attachment.initialLayout  = vk::ImageLayout::eUndefined;
	color_attachment.finalLayout    = vk::ImageLayout::ePresentSrcKHR;

	vk::AttachmentDescription depth_attachment;
	depth_attachment.format  = vk::Format::eD32Sfloat;
	depth_attachment.samples = vk::SampleCountFlagBits::e1;
	depth_attachment.loadOp  = vk::AttachmentLoadOp::eClear;
	depth_attachment.storeOp = vk::AttachmentStoreOp::eDontCare;
	depth_attachment.stencilLoadOp  = vk::AttachmentLoadOp::eDontCare;
	depth_attachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
	depth_attachment.initialLayout  = vk::ImageLayout::eUndefined;
	depth_attachment.finalLayout    = vk::ImageLayout::eDepthStencilAttachmentOptimal;

	vk::AttachmentReference color_attachment_reference;
	color_attachment_reference.attachment = 0;
	color_attachment_reference.layout = vk::ImageLayout::eColorAttachmentOptimal;

	vk::AttachmentReference depth_attachment_reference;
	depth_attachment_reference.attachment = 1;
	depth_attachment_reference.layout = vk::ImageLayout::eDepthStencilAttachmentOptimal;

	vk::SubpassDescription subpass;
	subpass.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &color_attachment_reference;
	subpass.pDepthStencilAttachment = &depth_attachment_reference;

	vk::SubpassDependency dependency;
	dependency.srcSubpass    = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass    = 0;
	dependency.srcStageMask  = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	dependency.srcAccessMask = (vk::AccessFlagBits)(0);
	dependency.dstStageMask  = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	dependency.dstAccessMask =	vk::AccessFlagBits::eColorAttachmentRead |
					vk::AccessFlagBits::eColorAttachmentWrite;

	array<vk::AttachmentDescription, 2> attachments = {
		color_attachment, depth_attachment
	};

	vk::RenderPassCreateInfo renderpass_info;
	renderpass_info.attachmentCount = static_cast<uint32_t>(attachments.size());
	renderpass_info.pAttachments = attachments.data();
	renderpass_info.subpassCount = 1;
	renderpass_info.pSubpasses = &subpass;
	renderpass_info.dependencyCount = 1;
	renderpass_info.pDependencies = &dependency;

	renderpass = device.createRenderPass(renderpass_info);
	if (!renderpass) {
		throw std::runtime_error("Failed to create renderpass object");
	}
}

void Application::CreateFramebuffers() {
	framebuffers.resize(swapchain_imageviews.size());
	for (size_t i = 0; i < swapchain_imageviews.size(); i++) {
		array<vk::ImageView, 2> attachments = {
			swapchain_imageviews[i], depth_imageview
		};

		vk::FramebufferCreateInfo framebuffer_info;
		framebuffer_info.renderPass = renderpass;
		framebuffer_info.attachmentCount = static_cast<uint32_t>(attachments.size());
		framebuffer_info.pAttachments = attachments.data();
		framebuffer_info.width = swapchain_extent.width;
		framebuffer_info.height = swapchain_extent.height;
		framebuffer_info.layers = 1;

		framebuffers[i] = device.createFramebuffer(framebuffer_info);
		if (!framebuffers[i]) {
			throw std::runtime_error("Failed to create swapchain framebuffer " + std::to_string(i));
		}
	}
}

void Application::CreateCommandPool() {
	VkQueueFamilyIndices indices(physical_device, surface);

	vk::CommandPoolCreateInfo pool_info;
	pool_info.queueFamilyIndex = indices.graphics;

	command_pool = device.createCommandPool(pool_info);
}

vk::CommandBuffer Application::BeginOneshotCommand() {
	vk::CommandBufferAllocateInfo alloc_info;
	alloc_info.level = vk::CommandBufferLevel::ePrimary;
	alloc_info.commandPool = command_pool;
	alloc_info.commandBufferCount = 1;

	vk::CommandBuffer command_buffer;
	command_buffer = device.allocateCommandBuffers(alloc_info)[0];

	vk::CommandBufferBeginInfo begin_info;
	begin_info.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit;

	command_buffer.begin(begin_info);
	return command_buffer;
}

void Application::EndOneshotCommand(vk::CommandBuffer command_buffer) {
	command_buffer.end();

	vk::SubmitInfo submit_info;
	submit_info.commandBufferCount = 1;
	submit_info.pCommandBuffers = &command_buffer;

	queue_graphics.submit({ submit_info }, nullptr);
	queue_graphics.waitIdle();

	device.freeCommandBuffers(command_pool, { command_buffer });
}

void Application::TransitionImageLayout(
	vk::Image image,
	vk::Format format,
	vk::ImageLayout old_layout,
	vk::ImageLayout new_layout,
	vk::ImageAspectFlags aspect_mask
) {

	vk::CommandBuffer command_buffer = BeginOneshotCommand();

	vk::PipelineStageFlags src_stage;
	vk::PipelineStageFlags dst_stage;

	vk::ImageMemoryBarrier barrier;
	barrier.oldLayout = old_layout;
	barrier.newLayout = new_layout;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.image = image;
	barrier.subresourceRange.aspectMask = aspect_mask;
	barrier.subresourceRange.baseMipLevel = 0;
	barrier.subresourceRange.levelCount = 1;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;

	if (
		old_layout == vk::ImageLayout::eUndefined and
		new_layout == vk::ImageLayout::eTransferDstOptimal
	) {
		barrier.srcAccessMask = (vk::AccessFlagBits)(0);
		barrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;

		src_stage = vk::PipelineStageFlagBits::eTopOfPipe;
		dst_stage = vk::PipelineStageFlagBits::eTransfer;

	} else if (
		old_layout == vk::ImageLayout::eTransferDstOptimal and
		new_layout == vk::ImageLayout::eShaderReadOnlyOptimal
	) {
		barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
		barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

		src_stage = vk::PipelineStageFlagBits::eTransfer;
		dst_stage = vk::PipelineStageFlagBits::eFragmentShader;

	} else if (
		old_layout == vk::ImageLayout::eUndefined and
		new_layout == vk::ImageLayout::eDepthStencilAttachmentOptimal
	) {
		barrier.srcAccessMask = (vk::AccessFlagBits)(0);
		barrier.dstAccessMask = vk::AccessFlagBits::eDepthStencilAttachmentRead |
					vk::AccessFlagBits::eDepthStencilAttachmentWrite;

		src_stage = vk::PipelineStageFlagBits::eTopOfPipe;
		dst_stage = vk::PipelineStageFlagBits::eEarlyFragmentTests;

	} else {
		throw std::runtime_error("Unsupported layout transition");
	}

	command_buffer.pipelineBarrier(
		src_stage, dst_stage,
		(vk::DependencyFlagBits)(0),
		{}, {}, {barrier}
	);

	EndOneshotCommand(command_buffer);
}

void Application::CreateDepthResources() {
	vk::Format depth_format = vk::Format::eD32Sfloat;

	vk::ImageCreateInfo image_info;
	image_info.imageType = vk::ImageType::e2D;
	image_info.extent.width = swapchain_extent.width;
	image_info.extent.height = swapchain_extent.height;
	image_info.extent.depth = 1;
	image_info.mipLevels = 1;
	image_info.arrayLayers = 1;
	image_info.format = depth_format;
	image_info.tiling = vk::ImageTiling::eOptimal;
	image_info.initialLayout = vk::ImageLayout::eUndefined;
	image_info.usage = vk::ImageUsageFlagBits::eDepthStencilAttachment;
	image_info.sharingMode = vk::SharingMode::eExclusive;
	image_info.samples = vk::SampleCountFlagBits::e1;
	depth_image = device.createImage(image_info);

	vk::MemoryRequirements mem_requirements;
	mem_requirements = device.getImageMemoryRequirements(depth_image);

	vk::MemoryAllocateInfo alloc_info;
	alloc_info.allocationSize = mem_requirements.size;
	alloc_info.memoryTypeIndex = FindMemoryType(
		mem_requirements.memoryTypeBits, vk::MemoryPropertyFlagBits::eDeviceLocal);

	depth_image_memory = device.allocateMemory(alloc_info);
	device.bindImageMemory(depth_image, depth_image_memory, 0);

	vk::ImageViewCreateInfo view_info;
	view_info.image = depth_image;
	view_info.viewType = vk::ImageViewType::e2D;
	view_info.format = depth_format;
	view_info.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eDepth;
	view_info.subresourceRange.baseMipLevel = 0;
	view_info.subresourceRange.levelCount = 1;
	view_info.subresourceRange.baseArrayLayer = 0;
	view_info.subresourceRange.layerCount = 1;

	depth_imageview = device.createImageView(view_info);

	TransitionImageLayout(
		depth_image, depth_format,
		vk::ImageLayout::eUndefined,
		vk::ImageLayout::eDepthStencilAttachmentOptimal,
		vk::ImageAspectFlagBits::eDepth
	);
}

void Application::AllocateCommandBuffers() {
	command_buffers.resize(framebuffers.size());

	vk::CommandBufferAllocateInfo alloc_info;
	alloc_info.commandPool = command_pool;
	alloc_info.level = vk::CommandBufferLevel::ePrimary;
	alloc_info.commandBufferCount = static_cast<uint32_t>(command_buffers.size());

	command_buffers = device.allocateCommandBuffers(alloc_info);

	for (size_t i = 0; i < command_buffers.size(); i++) {
		vk::CommandBufferBeginInfo begin_info;
		begin_info.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse;

		command_buffers[i].begin(begin_info);

		array<VkClearValue, 2> clear_values;
		clear_values[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
		clear_values[1].depthStencil = {1.0f, 0};

		vk::RenderPassBeginInfo renderpass_info;
		renderpass_info.renderPass = renderpass;
		renderpass_info.framebuffer = framebuffers[i];
		renderpass_info.renderArea.offset = vk::Offset2D(0, 0);
		renderpass_info.renderArea.extent = swapchain_extent;
		renderpass_info.clearValueCount = static_cast<uint32_t>(clear_values.size());
		renderpass_info.pClearValues = (const vk::ClearValue*)clear_values.data();
		command_buffers[i].beginRenderPass(renderpass_info, vk::SubpassContents::eInline);

		geometry.RecordDrawingCommands(command_buffers[i]);

		command_buffers[i].endRenderPass();
		command_buffers[i].end();
	}
}

void Application::UpdateUniformBuffer() {
	static auto start_time = std::chrono::high_resolution_clock::now();

	auto current_time = std::chrono::high_resolution_clock::now();
	float elapsed_time = 	std::chrono::duration<float, std::chrono::seconds::period>
				(current_time - start_time).count();
	UniformBufferObject ubo = {};
	ubo.model = glm::rotate(
			glm::mat4(1.0f),			// identity
			elapsed_time * glm::radians(90.0f),	// 90º per second
			glm::vec3(0.0f, 0.0f, 1.0f)		// rotate around (0,0,1)
	);

	ubo.view = glm::lookAt(
		glm::vec3(2.0f, 2.0f, 2.0f),	// camera position
		glm::vec3(0.0f, 0.0f, 0.0f),	// target position
		glm::vec3(0.0f, 0.0f, 1.0f)	// up direction
	);

	ubo.projection = glm::perspective(
		glm::radians(45.0f),
		swapchain_extent.width / (float)swapchain_extent.height,
		0.1f, 10.0f
	);
	ubo.projection[1][1] *= -1.0f;

	void *data;
	data = device.mapMemory(geometry.material.uniform_buffer_memory, 0, sizeof(ubo));
	std::memcpy(data, &ubo, sizeof(ubo));
	device.unmapMemory(geometry.material.uniform_buffer_memory);
}

void Application::CreateSemaphores() {
	vk::SemaphoreCreateInfo sem_info;
	sem_image_available = device.createSemaphore(sem_info);
	sem_render_finished = device.createSemaphore(sem_info);
}

void Application::DrawFrame() {
	queue_present.waitIdle();

	uint32_t image_index;
	device.acquireNextImageKHR(swapchain, UINT64_MAX, sem_image_available, nullptr, &image_index);

	vk::Semaphore wait_sem[] = { sem_image_available };
	vk::PipelineStageFlags wait_stages[] = { vk::PipelineStageFlagBits::eColorAttachmentOutput };

	vk::Semaphore signal_sem[] = { sem_render_finished };

	vk::SubmitInfo submit_info;
	submit_info.waitSemaphoreCount = 1;
	submit_info.pWaitSemaphores = wait_sem;
	submit_info.pWaitDstStageMask = wait_stages;
	submit_info.commandBufferCount = 1;
	submit_info.pCommandBuffers = &command_buffers[image_index];
	submit_info.signalSemaphoreCount = 1;
	submit_info.pSignalSemaphores = signal_sem;

	queue_graphics.submit({ submit_info }, nullptr);

	vk::SwapchainKHR swapchains[] = { swapchain };

	vk::PresentInfoKHR present_info;
	present_info.waitSemaphoreCount = 1;
	present_info.pWaitSemaphores = signal_sem;
	present_info.swapchainCount = 1;
	present_info.pSwapchains = swapchains;
	present_info.pImageIndices = &image_index;

	queue_present.presentKHR(present_info);
}
