#include "geometry.hpp"

#include <stdexcept>

#include "application.hpp"

using std::vector;
using std::array;

vk::VertexInputBindingDescription Vertex::GetBindingDescription() {
	vk::VertexInputBindingDescription description;
	description.binding = 0;
	description.stride = sizeof(Vertex);
	description.inputRate = vk::VertexInputRate::eVertex;

	return description;
}

array<vk::VertexInputAttributeDescription, 2> Vertex::GetAttributeDescription() {
	array<vk::VertexInputAttributeDescription, 2> descriptions;
	descriptions[0].binding = 0;
	descriptions[0].location = 0;
	descriptions[0].format = vk::Format::eR32G32B32Sfloat;
	descriptions[0].offset = offsetof(Vertex, position);

	descriptions[1].binding = 0;
	descriptions[1].location = 1;
	descriptions[1].format = vk::Format::eR32G32B32Sfloat;
	descriptions[1].offset = offsetof(Vertex, color);

	return descriptions;
}

void Geometry::CreateVertexBuffer() {
	vk::BufferCreateInfo buffer_info;
	buffer_info.size = sizeof(vertices[0]) * vertices.size();
	buffer_info.usage = vk::BufferUsageFlagBits::eVertexBuffer;
	buffer_info.sharingMode = vk::SharingMode::eExclusive;

	auto device = Application::get_device();
	vertex_buffer = device.createBuffer(buffer_info);

	vk::MemoryRequirements mem_requirements;
	mem_requirements = device.getBufferMemoryRequirements(vertex_buffer);

	vk::MemoryAllocateInfo alloc_info;
	alloc_info.allocationSize = mem_requirements.size;
	alloc_info.memoryTypeIndex = FindMemoryType(
		mem_requirements.memoryTypeBits,
		vk::MemoryPropertyFlagBits::eHostVisible |
		vk::MemoryPropertyFlagBits::eHostCoherent
	);

	vertex_buffer_memory = device.allocateMemory(alloc_info);
	device.bindBufferMemory(vertex_buffer, vertex_buffer_memory, 0);

	void *data;
	data = device.mapMemory(vertex_buffer_memory, 0, buffer_info.size);
	std::memcpy(data, vertices.data(), (size_t)buffer_info.size);
	device.unmapMemory(vertex_buffer_memory);
}

void Geometry::CreateIndexBuffer() {
	vk::BufferCreateInfo buffer_info;
	buffer_info.size = sizeof(indices[0]) * indices.size();
	buffer_info.usage = vk::BufferUsageFlagBits::eIndexBuffer;
	buffer_info.sharingMode = vk::SharingMode::eExclusive;

	auto device = Application::get_device();
	index_buffer = device.createBuffer(buffer_info);

	vk::MemoryRequirements mem_requirements;
	mem_requirements = device.getBufferMemoryRequirements(index_buffer);

	vk::MemoryAllocateInfo alloc_info;
	alloc_info.allocationSize = mem_requirements.size;
	alloc_info.memoryTypeIndex = FindMemoryType(
		mem_requirements.memoryTypeBits,
		vk::MemoryPropertyFlagBits::eHostVisible |
		vk::MemoryPropertyFlagBits::eHostCoherent
	);

	index_buffer_memory = device.allocateMemory(alloc_info);
	device.bindBufferMemory(index_buffer, index_buffer_memory, 0);

	void *data;
	data = device.mapMemory(index_buffer_memory, 0, buffer_info.size);
	std::memcpy(data, indices.data(), (size_t)buffer_info.size);
	device.unmapMemory(index_buffer_memory);
}

void Geometry::RecordDrawingCommands(vk::CommandBuffer &command_buffer) {
	command_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, material.GetPipeline());
	command_buffer.bindVertexBuffers(0, { vertex_buffer }, { 0 });
	command_buffer.bindIndexBuffer(index_buffer, 0, vk::IndexType::eUint16);
	command_buffer.bindDescriptorSets(
		vk::PipelineBindPoint::eGraphics,
		material.pipeline_layout, 0,
		{material.descriptor_set}, {}
	);

	command_buffer.drawIndexed(static_cast<uint32_t>(indices.size()), 1, 0, 0, 0);
}

void Geometry::Cleanup() {
	auto device = Application::get_device();

	device.destroyBuffer(index_buffer);
	device.freeMemory(index_buffer_memory);

	device.destroyBuffer(vertex_buffer);
	device.freeMemory(vertex_buffer_memory);

	material.Cleanup();
}
