#pragma once

#include <vector>
#include <string>

#include <vulkan/vulkan.hpp>

class Shader {
protected:
	std::vector<char> code_vertex;
	std::vector<char> code_fragment;

	vk::ShaderModule CreateModule(const std::vector<char> &code);
public:
	vk::ShaderModule module_vertex;
	vk::ShaderModule module_fragment;

	Shader();
	Shader(
		const std::string& filename_vertex_shader,
		const std::string& filename_fragment_shader
	);

	void LoadVertexShader(const std::string& filename);
	void LoadFragmentShader(const std::string& filename);

	void DestroyModules();
};
