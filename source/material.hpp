#pragma once

#include <string>
#include <vector>

#include <glm/glm.hpp>

#include "shader.hpp"

struct UniformBufferObject {
	glm::mat4 model;
	glm::mat4 view;
	glm::mat4 projection;
};

class Material {
protected:
	Shader shader;
	std::string entrypoint_vertex = "main";
	std::string entrypoint_fragment = "main";

	vk::DescriptorSetLayout descriptor_set_layout;
	vk::Pipeline pipeline;

	vk::Buffer uniform_buffer;

	vk::DescriptorPool descriptor_pool;

public:
	// Vertex Input
	vk::PrimitiveTopology topology = vk::PrimitiveTopology::eTriangleList;
	bool primitive_restart_enable = false;

	// Rasterizer
	vk::CullModeFlags culling = vk::CullModeFlagBits::eBack;
	vk::FrontFace front_face = vk::FrontFace::eClockwise;
	vk::PolygonMode polygon_mode = vk::PolygonMode::eFill;
	float line_width = 1.0f; // >1 requires 'wideLines' GPU feature enabled

	// Geometry accesses these in 'RecordDrawingCommands'
	vk::PipelineLayout pipeline_layout;
	vk::DescriptorSet descriptor_set;
	
	vk::DeviceMemory uniform_buffer_memory;

	void SetShader(const Shader&);
	void SetVertexShaderEntryPoint(const std::string&);
	void SetFragmentShaderEntryPoint(const std::string&);

	Shader& GetShader();
	vk::Pipeline& GetPipeline();

	void CreateDescriptorSetLayout();
	void CreateUniformBuffer();
	void CreateDescriptorPool();
	void CreateDescriptorSet();
	void CreateGraphicsPipeline(
		const std::vector<vk::Viewport> &viewports,
		const std::vector<vk::Rect2D>   &scissors,
		vk::RenderPass renderpass,
		bool enable_multisampling = false,
		vk::SampleCountFlagBits sample_count = vk::SampleCountFlagBits::e1
	);

	void Cleanup();
};
