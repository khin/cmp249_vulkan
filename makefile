##################################################
#
#	Makefile template
#		by Khin Baptista
#
##################################################

CC      = g++
LINKER  = g++
CGLSL   = glslangValidator

CFLAGS  = -Wall -std=c++14
LDFLAGS = -lvulkan
GLFLAGS = -V100

DEBUG = 1
DEBUG_FLAGS = -g -ggdb -D_DEBUG_ENABLED_

##################################################

# Name of the project (executable binary)
Project	= cmp249-vulkan

# List of packages to use with 'pkg-config'
Packages = glfw3

# Path to directories
SourcePath  = source
ObjectsPath = source/objects

# Path to place compiled shaders
ShadersPath = shaders

# Source files names
SourceFiles = main.cpp application.cpp \
	shader.cpp material.cpp geometry.cpp

# Shader source files (GLSL)
ShaderFiles = shader.vert shader.frag

##################################################

CPP = $(patsubst %, $(SourcePath)/%, $(SourceFiles))
OBJ = $(patsubst $(SourcePath)/%.cpp, $(ObjectsPath)/%.o, $(CPP))
DEP = $(patsubst %.o, %.d, $(OBJ))

GLSL = $(patsubst %, $(SourcePath)/%, $(ShaderFiles))
VERT = $(filter %.vert, $(GLSL))
FRAG = $(filter %.frag, $(GLSL))

SPIRV  = $(patsubst $(SourcePath)/%.vert, $(ShadersPath)/%-vert.spv, $(VERT))
SPIRV += $(patsubst $(SourcePath)/%.frag, $(ShadersPath)/%-frag.spv, $(FRAG))

CFLAGS = `pkg-config --cflags $(Packages)`
LDFLAGS += `pkg-config --static --libs $(Packages)`

ifeq ($(DEBUG), 0)
CFLAGS += -DNDEBUG
else
CFLAGS += $(DEBUG_FLAGS)
endif

##################################################

.PHONY: all clean

all: objectdir shaders $(Project)

objectdir:
	mkdir -p $(ObjectsPath)

test: all
	./$(Project)

remake: clean all

$(Project): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

-include $(DEP)
$(ObjectsPath)/%.o: $(SourcePath)/%.cpp
	$(CC) -MMD -c -o $@ $< $(CFLAGS)

clean:
	rm -f $(ObjectsPath)/* $(Project) $(ShadersPath)/*.spv
	rmdir $(ObjectsPath)

##################################################

shaders: $(SPIRV)

$(ShadersPath)/%-frag.spv: $(SourcePath)/%.frag
	$(CGLSL) -o $@ $(GLFLAGS) $^

$(ShadersPath)/%-vert.spv: $(SourcePath)/%.vert
	$(CGLSL) -o $@ $(GLFLAGS) $^

##################################################
